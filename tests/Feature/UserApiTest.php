<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserApiTest extends TestCase
{
    use RefreshDatabase;

    private $username = 'andrigunawan';
    private $password = '';

    public function test_register()
    {
        $formData = array(
            'username' => $this->username,
            'role' => 'Software Engineer'
        );

        $response = $this->post('api/register', $formData);

        $this->password = $response->decodeResponseJson()['response']['password'];

        $response->assertStatus(200);
    }

    public function test_login()
    {

        $formData = array(
            'username' => $this->username,
            'password' => $this->password
        );

        $response = $this->post('api/login', $formData);

        $response->assertStatus(200);
    }
}
