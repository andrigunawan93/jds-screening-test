<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *   title="JDS Screening Test Simple App",
 *   version="1.0",
 *   description="This is an API for Screening Test JDS",
 *   @OA\Contact(
 *     email="dev.andrigunawan@gmail.com",
 *     name="Developer"
 *   )
 * )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
