<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function all()
    {
        $users = User::all();

        return response()->json(['metaData' => ['code' => 200, 'message' => 'OK.'], 'response' => $users], 200);
    }

    public function show($id)
    {
        $user = User::find($id);

        return response()->json(['metaData' => ['code' => 200, 'message' => 'OK.'], 'response' => $user], 200);
    }
}