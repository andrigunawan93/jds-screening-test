<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Validator;
use App\Models\User;
use Helper;

class AuthController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/register",
     *     description="Register",
     *     tags={"Authentication"},
     *     @OA\RequestBody(
     *         required=true,
     *           @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *         @OA\Property(
     *           property="username",
     *           description="username",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="role",
     *           description="role",
     *           type="string",
     *         ),
     *       ),
     *     ),
     *     ),
     *     @OA\Response(response="200", description="Register")
     * )
     */

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'username' => 'required|string|max:50|unique:users',
            'role' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json(['metaData' => ['code' => 402, 'message' => $validator->errors()], 'response' => null], 200);    
        }

        $password = Helper::generatePassword(6);

        $user = User::create([
            'username' => $request->username,
            'role' => $request->role,
            'password' => Hash::make($password)
        ]);

        $body = array(
            'username' => $user->username,
            'role' => $user->role,
            'password' => $password
,        );

        return response()->json(['metaData' => ['code' => 200, 'message' => 'OK.'], 'response' => $body], 200);
    }

    /**
     * @OA\Post(
     *     path="/api/login",
     *     description="Login",
     *     tags={"Authentication"},
     *     @OA\RequestBody(
     *         required=true,
     *           @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *         @OA\Property(
     *           property="username",
     *           description="username",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="password",
     *           description="password",
     *           type="string",
     *         ),
     *       ),
     *     ),
     *     ),
     *     @OA\Response(response="200", description="Login")
     * )
     */

    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('username', 'password')))
        {
            return response()
                ->json(['message' => 'Unauthorized'], 401);
        }

        $user = User::where('username', $request['username'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        $body = array(
            'id' => $user->id,
            'username' => $user->username,
            'token' => $token
        );

        return response()->json(['metaData' => ['code' => 200, 'message' => 'OK.'], 'response' => $body], 200);
    }

    /**
     * @OA\Post(
     *     path="/api/validate-token",
     *     description="Validate Token",
     *     tags={"Access Token"},
     *     @OA\RequestBody(
     *         required=true,
     *           @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *         @OA\Property(
     *           property="token",
     *           description="token",
     *           type="string",
     *         )
     *       ),
     *     ),
     *     ),
     *     @OA\Response(response="200", description="Validate Token")
     * )
     */

    public function validateToken(Request $request)
    {
        $body = array(
            'is_valid' => false,
            'expired_at' => null,
            'username' => null
        );

        $token = \Laravel\Sanctum\PersonalAccessToken::findToken($request['token']);

        if($token != null) {
            $user = User::find($token->tokenable_id);

            $body['is_valid'] = true;
            $body['expired_at'] = date('Y-m-d H:i:s', strtotime($token->created_at->addDays(1)));
            $body['username'] = $user->username;
        }

        return response()->json($body, 200);
    }

    // method for user logout and delete token
    public function logout()
    {
        auth()->user()->tokens()->delete();

        return [
            'message' => 'You have successfully logged out and the token was successfully deleted'
        ];
    }
}