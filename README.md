<p align="center"><img src="https://jabarprov.go.id/assets_front/images/logo.png" width="50"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Simple App for Screening Test JDS

##  Server Requirements

- PHP >= 7.2.5
- PostgreSQL

## Staging Version

- composer install
- cp .env.staging .env
- Config database connection in .env
- php artisan key:generate
- php artisan migrate

## Production Version

- composer install
- cp .env.staging .env
- Config database connection in .env
- php artisan key:generate
- php artisan migrate

## Swagger Documentation

http://127.0.0.1:8000/api/documentation

## Live Demo

http://test-jds.andrigunawan.com

