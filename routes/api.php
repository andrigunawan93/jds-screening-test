<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/', function ($id) {
    return '';
});

//API route for register new user
Route::post('/register', [App\Http\Controllers\API\AuthController::class, 'register']);
//API route for login user
Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login']);

Route::post('/validate-token', [App\Http\Controllers\API\AuthController::class, 'validateToken']);

//Protecting Routes
Route::group(['middleware' => ['auth:sanctum']], function () {
    // API route for users
    Route::get('/user', [App\Http\Controllers\API\UserController::class, 'all']);
    Route::get('/user/{id}', [App\Http\Controllers\API\UserController::class, 'show']);

    // API route for logout user
    Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);
});